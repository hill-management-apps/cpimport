﻿
namespace CPImport
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChoose = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnImportTrans = new System.Windows.Forms.Button();
            this.btnChooseTrans = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtBankDepFileName = new System.Windows.Forms.TextBox();
            this.txtDetailedTransFileName = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btnChoose
            // 
            this.btnChoose.Location = new System.Drawing.Point(20, 69);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(75, 23);
            this.btnChoose.TabIndex = 0;
            this.btnChoose.Text = "Choose File";
            this.btnChoose.UseVisualStyleBackColor = true;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(20, 122);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(541, 44);
            this.label1.TabIndex = 3;
            this.label1.Text = "Import ClickPay Bank Deposits";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(101, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(302, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "First, Choose Bank Deposits Import";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(101, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Import Bank Deposits";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xls";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel Files (*.xls)|*.xls;*.xlsx";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(665, 44);
            this.label5.TabIndex = 8;
            this.label5.Text = "Import ClickPay Detailed Transactions";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label7.Location = new System.Drawing.Point(101, 342);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(247, 24);
            this.label7.TabIndex = 13;
            this.label7.Text = "Import Detailed Transactions";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label8.Location = new System.Drawing.Point(101, 289);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(414, 24);
            this.label8.TabIndex = 12;
            this.label8.Text = "First, Choose the Detailed Transactions to Import";
            // 
            // btnImportTrans
            // 
            this.btnImportTrans.Location = new System.Drawing.Point(20, 342);
            this.btnImportTrans.Name = "btnImportTrans";
            this.btnImportTrans.Size = new System.Drawing.Size(75, 23);
            this.btnImportTrans.TabIndex = 10;
            this.btnImportTrans.Text = "Import";
            this.btnImportTrans.UseVisualStyleBackColor = true;
            this.btnImportTrans.Click += new System.EventHandler(this.btnImportTrans_Click);
            // 
            // btnChooseTrans
            // 
            this.btnChooseTrans.Location = new System.Drawing.Point(20, 289);
            this.btnChooseTrans.Name = "btnChooseTrans";
            this.btnChooseTrans.Size = new System.Drawing.Size(75, 23);
            this.btnChooseTrans.TabIndex = 9;
            this.btnChooseTrans.Text = "Choose File";
            this.btnChooseTrans.UseVisualStyleBackColor = true;
            this.btnChooseTrans.Click += new System.EventHandler(this.btnChooseTrans_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(594, 393);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Done";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtBankDepFileName
            // 
            this.txtBankDepFileName.BackColor = System.Drawing.SystemColors.Control;
            this.txtBankDepFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBankDepFileName.ForeColor = System.Drawing.Color.Firebrick;
            this.txtBankDepFileName.Location = new System.Drawing.Point(105, 96);
            this.txtBankDepFileName.Name = "txtBankDepFileName";
            this.txtBankDepFileName.Size = new System.Drawing.Size(564, 13);
            this.txtBankDepFileName.TabIndex = 14;
            // 
            // txtDetailedTransFileName
            // 
            this.txtDetailedTransFileName.BackColor = System.Drawing.SystemColors.Control;
            this.txtDetailedTransFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDetailedTransFileName.ForeColor = System.Drawing.Color.Firebrick;
            this.txtDetailedTransFileName.Location = new System.Drawing.Point(105, 316);
            this.txtDetailedTransFileName.Name = "txtDetailedTransFileName";
            this.txtDetailedTransFileName.Size = new System.Drawing.Size(564, 13);
            this.txtDetailedTransFileName.TabIndex = 15;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 393);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(557, 23);
            this.progressBar1.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 428);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.txtDetailedTransFileName);
            this.Controls.Add(this.txtBankDepFileName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnImportTrans);
            this.Controls.Add(this.btnChooseTrans);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnChoose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ClickPay File Import";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnImportTrans;
        private System.Windows.Forms.Button btnChooseTrans;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBankDepFileName;
        private System.Windows.Forms.TextBox txtDetailedTransFileName;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

