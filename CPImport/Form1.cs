﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using ExcelDataReader;
using Utilities;
using System.Reflection;

namespace CPImport
{
    public enum ProcessType
    {
        OkToInsert = 0,
        DeleteFirst,
        Skip,
        None
    }

    public partial class Form1 : Form
    {
        public string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;IMEX=1;HDR=YES\";";

        private string importBankDepFileName = string.Empty;
        private string importDetailedTransFileName = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                importBankDepFileName = openFileDialog1.FileName;
                txtBankDepFileName.Text = importBankDepFileName;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            int rowsInserted = 0;
            int rowsSkipped = 0;
            int rowsTotal = 0;
            string sqlInsertFormat =
                "insert into HS_ClickPay_Deposit_RM " +
                "values('{0}', '{1}', '{2}', {3}, {4}, {5}, {6}, " +
                "         {7}, {8}, {9}, {10}, {11}, {12}, {13}, " +
                "         {14}, {15}, {16} " +
                "       ) ";

            string cpDate = string.Empty;
            string cpMerchant = string.Empty;
            decimal cpTotAmt = 0;        // substring skips the '$'

            StringBuilder sbSqlInsert = new StringBuilder();
            SqlAgent sa = new SqlAgent();
            string sqlInsert = string.Empty;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (importBankDepFileName.Length == 0)
                {
                    throw new Exception("You must first choose a Detailed Transaction Spreadsheet before attempting to Import.");
                }

                using (var stream = File.Open(importBankDepFileName, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader reader;
                    reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = false
                        }
                    };

                    DataSet ds = reader.AsDataSet(conf);
                    DataTable dt = ds.Tables[0];

                    // skipCount is used to skip the first two rows of headers.
                    int skipCount = 0;
                    progressBar1.Minimum = 0;
                    progressBar1.Maximum = dt.Rows.Count;
                    progressBar1.Value = 0;

                    foreach (DataRow dr in dt.Rows)
                    {
                        progressBar1.Value++;

                        rowsTotal++;            // Count the total rows
                        //if (rowsTotal == 171)
                        //{
                        //    int i = 0;
                        //}

                        if (skipCount > 1)
                        {
                            // parse Date, Merchant and TotalAmount from the input
                            // This represents a kind of unique key
                            cpDate = dr[0].ToString();
                            if (cpDate == "Total:")     // The very last line is a total line, do not process that line.
                                continue;

                            cpMerchant = dr[2].ToString();

                            string tmp = dr[4].ToString();
                            if (tmp.IndexOf('-') >= 0)
                                cpTotAmt = decimal.Parse(tmp.Substring(2));        // substring skips the '-' AND the '$'
                            else
                                cpTotAmt = decimal.Parse(tmp.Substring(1));        // substring skips the '$' only


                            // select a count of rows in the MRI.HOMESALES.HS_ClickPay_Deposit table
                            // where date, merchang and total amount is equal to the input row.
                            string sql = string.Format("select count(*) from HS_ClickPay_Deposit_RM where date = '{0}' and merchant = '{1}' and TotalAmount = {2}",
                                cpDate, cpMerchant, cpTotAmt);

                            DataSet dsMRI = sa.ExecuteQuery(sql);


                            // If the count is greater than zero, the row already exists in MRI
                            // If the count equals zero, INSERT the row from the input table into MRI.HOMESALES.hs_clickPay_Deposit_RM
                            int rowsFound = int.Parse(dsMRI.Tables[0].Rows[0][0].ToString());
                            if (rowsFound == 0)
                            {
                                if (dr.Table.Columns.Count == 9) // It's a remote payment
                                {
                                    sqlInsert = string.Format(sqlInsertFormat, cpDate, dr[1].ToString(), cpMerchant, dr[3].ToString(), cpTotAmt,
                                        int.Parse(dr[5].ToString()), FixDecimal(dr[6].ToString()),
                                        int.Parse(dr[7].ToString()), FixDecimal(dr[8].ToString()),
                                        0, 0, 0, 0, 0, 0, 0, 0);
                                }
                                else
                                {
                                    sqlInsert = string.Format(sqlInsertFormat, cpDate, dr[1].ToString(), cpMerchant, dr[3].ToString(), cpTotAmt,
                                        int.Parse(dr[5].ToString()), FixDecimal(dr[6].ToString()),
                                        int.Parse(dr[7].ToString()), FixDecimal(dr[8].ToString()),
                                        int.Parse(dr[9].ToString()), FixDecimal(dr[10].ToString()),
                                        int.Parse(dr[11].ToString()), FixDecimal(dr[12].ToString()),
                                        int.Parse(dr[13].ToString()), FixDecimal(dr[14].ToString()),
                                        int.Parse(dr[15].ToString()), FixDecimal(dr[16].ToString()));
                                }
                                // Insert a row
                                //sa.ExecuteNonQuery(sqlInsert);
                                sbSqlInsert.Append(sqlInsert);
                                rowsInserted++;     // Cuont the number of rows inserted
                            }
                            else
                            {
                                // Skip this row
                                rowsSkipped++;      // Count the number of rows skipped
                            }

                            // Continue looping
                        }
                        else
                            skipCount++;

                    }

                    if (sbSqlInsert.ToString().Length > 0)
                        sa.ExecuteNonQuery(sbSqlInsert.ToString());
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Message: {0} - Date: {1} - Merchant: {2}, - Amount: {3} - RowNumber: {4}",
                    ex.Message, cpDate, cpMerchant, cpTotAmt, rowsTotal);

                MessageBox.Show(msg, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            MessageBox.Show(string.Format("Process Complete!  Rows Processed: {0}  -  Rows Inserted: {1}  -  Rows Skipped: {2}", 
                rowsTotal, rowsInserted, rowsSkipped), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private decimal FixDecimal(string dec)
        {
            // Here's the thing.  decimal.Parse doesn't work on values that begin with a '$'.
            // I fixed this with the use of Substring.
            // However, if the number is negative, substring has to start in position 2.
            //
            // This function does that.
            //
            if (dec.IndexOf("-") >= 0)
                return decimal.Parse(dec.Substring(2));
            else
                return decimal.Parse(dec.Substring(1));
        }


        //========================================
        //====================================================================================================
        // This bottom area has code migrated from the program ImportClickPayDetailed.  That program will  ===
        // be retired ahd THIS program will import from both available sources.                            ===
        //====================================================================================================
        //========================================


        private void ImportDetailedTransRows()
        {
            const string SQL_FORMAT = "('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', '{32}'), ";
            const string SQL_FORMAT_NULL_SETT_DATE = "('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}', '{13}', '{14}', {15}, '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', '{32}'), ";
            string finalSqlFormat = string.Empty;

            DataSet ds = null;
            DataTable dt = null;

            SqlAgent sa = new SqlAgent();
            string sql = string.Empty;

            int RowsAdded = 0;
            int RowsRejected = 0;


            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (importDetailedTransFileName.Length == 0)
                {
                    throw new Exception("You must first choose a Detailed Transaction Spreadsheet before attempting to Import.");
                }

                using (var stream = File.Open(importDetailedTransFileName, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader reader;
                    reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = false
                        }
                    };

                    ds = reader.AsDataSet(conf);
                }

                dt = ds.Tables[0];

                // This is the beginning of the SQL statement ONLY.  Each time a batch is executed,
                // It will revert back to this.
                string sqlBegin = "insert into hs_clickpay_detailed_RM ( " +
                    "[LLC_Association] " +
                    ",[Date] " +
                    ",[Property] " +
                    ",[Building_ID] " +
                    ",[Resident_Name] " +
                    ",[Payer] " +
                    ",[Email_Address] " +
                    ",[Ledger_Name] " +
                    ",[Site] " +
                    ",[Confirmation_Number] " +
                    ",[Amount] " +
                    ",[Merchant_Fees] " +
                    ",[Reason_Code] " +
                    ",[Comments] " +
                    ",[Transaction_Detail_Comments] " +
                    ",[Settlement_Date] " +
                    ",[Description] " +
                    ",[Phone] " +
                    ",[Payment_Method] " +
                    ",[Account_Number] " +
                    ",[Account_Token] " +
                    ",[Product_Description] " +
                    ",[Product_ID] " +
                    ",[Merchant] " +
                    ",[Merchant_Tag] " +
                    ",[CheckNumber] " +
                    ",[GL_Code] " +
                    ",[Sub_Account] " +
                    ",[Lockbox_Check_Type] " +
                    ",[Account_Created_By] " +
                    ",[Last_4_Digits__Payment_Method_] " +
                    ",[Batch_ID__ClickPay_] " +
                    ",[Software_BatchID]) VALUES ";

                sql = sqlBegin;
                bool firstTimeThru = true;
                int counter = 1;

                progressBar1.Minimum = 0;
                progressBar1.Maximum = dt.Rows.Count;
                progressBar1.Value = 0;

                // Loop thru resulting dataset.
                foreach (DataRow dr in dt.Rows)
                {
                    progressBar1.Value++;
                    // skip the first row of headers.
                    if (firstTimeThru)
                    {
                        firstTimeThru = false;
                        continue;
                    }

                    // /// DEBUG ///
                    if (dr[4].ToString().Trim() == "Portillo, Leza")
                    {
                        int i = 0;
                    }

                    // Making sure the single quote in names doesn't play hob with the SQL statement.
                    if (dr[4].ToString().Contains("'"))
                    {
                        dr[4] = dr[4].ToString().Replace("'", "`");
                    }
                    if (dr[5].ToString().Contains("'"))
                    {
                        dr[5] = dr[5].ToString().Replace("'", "`");
                    }
                    if (dr[7].ToString().Contains("'"))
                    {
                        dr[7] = dr[7].ToString().Replace("'", "`");
                    }


                    int iTmp = dr[1].ToString().IndexOf(" ", 14);
                    string sTmp = dr[1].ToString().Substring(0, iTmp);
                    DateTime dtDate = DateTime.Parse(sTmp);
                    string sDate = dtDate.ToString("MM/dd/yyyy hh:mm:ss");

                    string sSett = string.Empty;
                    if (dr[15].ToString() == " ")    // It's an empty cell in Excel
                    {
                        sSett = "NULL";
                        finalSqlFormat = SQL_FORMAT_NULL_SETT_DATE;
                        //continue;
                    }
                    else
                    {
                        iTmp = dr[15].ToString().IndexOf(" ", 14);
                        sTmp = dr[15].ToString().Substring(0, iTmp);
                        DateTime dtSett = DateTime.Parse(sTmp);
                        sSett = dtSett.ToString("MM/dd/yyyy hh:mm:ss");
                        finalSqlFormat = SQL_FORMAT;
                    }

                    // Formatting the Amount into a number.  It might be negative, hense the check for a "-".
                    decimal amt = 0;
                    if (dr[10].ToString().IndexOf("-") >= 0)
                        amt = decimal.Parse(string.Format("-{0}", dr[10].ToString().Substring(2)));
                    else
                        amt = decimal.Parse(dr[10].ToString().Substring(1));

                    // If the batchID is null or empty, look it up
                    string batchID = dr[32].ToString();
                    if (batchID == "")
                    {
                        batchID = FetchMissingBatchID(dr[9].ToString());
                    }

                    // Determine if a row with matching confirmation number already exists
                    ProcessType pt = DetermineConfirmationAndSettlementDate(dr[9].ToString(), sSett);

                    if (pt == ProcessType.DeleteFirst)
                    {
                        SqlAgent saDelete = new SqlAgent();
                        sa.ExecuteNonQuery(string.Format("delete from hs_clickpay_detailed_RM where Confirmation_Number = '{0}' ", dr[9].ToString()));
                        saDelete = null;
                    }


                    if (pt == ProcessType.DeleteFirst || pt == ProcessType.OkToInsert)
                    {
                        sql += string.Format(finalSqlFormat,
                            dr[0].ToString().Length > 50 ? dr[0].ToString().Substring(0, 50) : dr[0].ToString(),
                            sDate,
                            dr[2].ToString().Length > 50 ? dr[2].ToString().Substring(0, 50) : dr[2].ToString(),
                            dr[3].ToString(),
                            dr[4].ToString().Length > 50 ? dr[4].ToString().Substring(0, 50) : dr[4].ToString(),
                            dr[5].ToString().Length > 50 ? dr[5].ToString().Substring(0, 50) : dr[5].ToString(),
                            dr[6].ToString().Length > 50 ? dr[6].ToString().Substring(0, 50) : dr[6].ToString(),
                            dr[7].ToString() == string.Empty ? " " : dr[7].ToString(),
                            dr[8].ToString().Length > 50 ? dr[8].ToString().Substring(0, 50) : dr[8].ToString(),
                            dr[9].ToString().Length > 50 ? dr[9].ToString().Substring(0, 50) : dr[9].ToString(),
                            amt.ToString(),
                            dr[11].ToString().Length > 50 ? dr[11].ToString().Substring(0, 50) : dr[11].ToString(),
                            dr[12].ToString().Length > 1 ? dr[12].ToString().Substring(0, 1) : dr[12].ToString(),
                            dr[13].ToString().Length > 100 ? dr[13].ToString().Substring(0, 100) : dr[13].ToString(),
                            dr[14].ToString().Length > 100 ? dr[14].ToString().Substring(0, 100) : dr[14].ToString(),
                            sSett,
                            dr[16].ToString().Length > 50 ? dr[16].ToString().Substring(0, 50) : dr[16].ToString(),
                            dr[17].ToString().Length > 50 ? dr[17].ToString().Substring(0, 50) : dr[17].ToString(),
                            dr[18].ToString().Length > 50 ? dr[18].ToString().Substring(0, 50) : dr[18].ToString(),
                            dr[19].ToString().Length > 50 ? dr[19].ToString().Substring(0, 50) : dr[19].ToString(),
                            dr[20].ToString().Length > 50 ? dr[20].ToString().Substring(0, 50) : dr[20].ToString(),
                            dr[21].ToString().Length > 1 ? dr[21].ToString().Substring(0, 1) : dr[21].ToString(),
                            dr[22].ToString().Length > 50 ? dr[22].ToString().Substring(0, 50) : dr[22].ToString(),
                            dr[23].ToString().Length > 50 ? dr[23].ToString().Substring(0, 50) : dr[23].ToString(),
                            dr[24].ToString().Length > 50 ? dr[24].ToString().Substring(0, 50) : dr[24].ToString(),
                            dr[25].ToString().Length > 1 ? dr[25].ToString().Substring(0, 1) : dr[25].ToString(),
                            dr[26].ToString().Length > 1 ? dr[26].ToString().Substring(0, 1) : dr[26].ToString(),
                            dr[27].ToString().Length > 1 ? dr[27].ToString().Substring(0, 1) : dr[27].ToString(),
                            dr[28].ToString().Length > 1 ? dr[28].ToString().Substring(0, 1) : dr[28].ToString(),
                            dr[29].ToString().Length > 50 ? dr[29].ToString().Substring(0, 50) : dr[29].ToString(),
                            dr[30].ToString(),
                            dr[31].ToString().Length > 50 ? dr[31].ToString().Substring(0, 50) : dr[31].ToString(),
                            batchID);

                        // Batch up the INSERT statements. 1000 is the max, so I'm stopping at 900.
                        if (counter % 900 == 0)
                        {
                            sql = sql.Substring(0, sql.Length - 2);     // Trying to strip the trailing comma.  Subtract 4 because of spaces after the comma.
                            sql += "; ";                                // Then add a trailing semi to indicate the end of the batch.

                            sa.ExecuteNonQuery(sql);

                            sql = sqlBegin;
                        }

                        counter++;
                        RowsAdded++;
                    }
                    else
                    {
                        // If it's NOT okToInsert, that means the confirmation nunmber DOES exist.  Skip the row.
                        RowsRejected++;
                    }
                }

                // Follow up with the finall batch
                sql = sql.Substring(0, sql.Length - 2);     // Trying to strip the trailing comma.  Subtract 4 because of spaces after the comma.
                sql += "; ";                                // Then add a trailing semi to indicate the end of the batch.

                if (RowsAdded > 0)
                    sa.ExecuteNonQuery(sql);

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("An error occured: {0}", ex.Message));
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            MessageBox.Show(string.Format("Detailed Transactions Successfully Processed.  Rows Added: {0}  --  Rows Skipped: {1}", RowsAdded, RowsRejected));
        }


        private static ProcessType DetermineConfirmationAndSettlementDate(string confNum, string settlementDate)
        {
            //logger.LogMessage("Here in DetermineConfirmationExists");

            try
            {
                SqlAgent sa = new SqlAgent();
                DataSet ds = sa.ExecuteQuery(string.Format("select * from hs_clickpay_detailed_RM where confirmation_number = '{0}'", confNum));


                if (ds.Tables[0].Rows.Count == 0)      // No row found for that confNum
                    return ProcessType.OkToInsert;
                else
                {
                    // Check first for the NULL value
                    if (settlementDate == "NULL")
                    {
                        if (ds.Tables[0].Rows[0][15].ToString() == DBNull.Value.ToString())
                            return ProcessType.Skip;
                        else
                            return ProcessType.DeleteFirst;
                    }

                    // Otherwise, parse up the dates and do the normal value cecking.
                    DateTime dtDetailed = DateTime.MinValue;
                    DateTime dtExcel = DateTime.MinValue;

                    if (ds.Tables[0].Rows[0][15].ToString() != string.Empty)
                        dtDetailed = DateTime.Parse(ds.Tables[0].Rows[0][15].ToString());

                    dtExcel = DateTime.Parse(settlementDate);

                    string sDetailed = string.Format("{0}{1}{2}{3}{4}",
                        dtDetailed.Year, dtDetailed.Month, dtDetailed.Day, dtDetailed.Minute, dtDetailed.Second);
                    string sExcel = string.Format("{0}{1}{2}{3}{4}",
                        dtExcel.Year, dtExcel.Month, dtExcel.Day, dtExcel.Minute, dtExcel.Second);

                    if (sDetailed == sExcel)
                        return ProcessType.Skip;
                    else
                        return ProcessType.DeleteFirst;
                }

            }
            catch(Exception ex)
            {
                throw;
            }
        }

        private static string FetchMissingBatchID(string confNum)
        {
            string retVal = string.Empty;   // carve out the actual conf num less the '/1'.
            string cNum = string.Empty;

            if (confNum.Substring(0, 1) == "L" || confNum.Substring(0, 1) == "R")
                cNum = confNum.Substring(0, 14);
            else
                cNum = confNum.Substring(0, 18);

            string sql = string.Format("select rmbatchid from rmledg where paymentranid like '{0}%'", cNum);

            try
            {
                SqlAgent sa = new SqlAgent();
                DataSet ds = sa.ExecuteQuery(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    retVal = ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                throw;
            }

            return retVal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChooseTrans_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                importDetailedTransFileName = openFileDialog1.FileName;
                txtDetailedTransFileName.Text = importDetailedTransFileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtBankDepFileName.Text = "Choose";
            txtDetailedTransFileName.Text = "Choose";

            Version version = Assembly.GetEntryAssembly().GetName().Version;
            this.Text = this.Text + " - V: " + version;
        }

        private void btnImportTrans_Click(object sender, EventArgs e)
        {
            ImportDetailedTransRows();
        }
    }
}
